---
title: "Changing the Game: Accelerating Applications and Improving Performance For Greater Data Center Efficiency"
date: "2015-01-16"
categories: 
  - "blogs"
---

### Abstract

Planning for exascale, accelerating time to discovery and extracting results from massive data sets requires organizations to continually seek faster and more efficient solutions to provision I/O and accelerate applications.  New burst buffer technologies are being introduced to address the long-standing challenges associated with the overprovisioning of storage by decoupling I/O performance from capacity. Some of these solutions allow large datasets to be moved out of HDD storage and into memory quickly and efficiently. Then, data can be moved back to HDD storage once processing is complete much more efficiently with unique algorithms that align small and large writes into streams, thus enabling users to implement the largest, most economical HDDs to hold capacity.

This type of approach can significantly reduce power consumption, increase data center density and lower system cost. It can also boost data center efficiency by reducing hardware, power, floor space and the number of components to manage and maintain. Providing massive application acceleration can also greatly increase compute ROI by returning wasted processing cycles to compute that were previously managing storage activities or waiting for I/O from spinning disk.

This session will explain how the latest burst buffer cache and I/O accelerator applications can enable organizations to separate the provisioning of peak and sustained performance requirements with up to 70 percent greater operational efficiency and cost savings than utilizing exclusively disk-based parallel file systems via a non-vendor-captive software-based approach.

### Speaker Bio

[Jeff Sisilli](https://www.linkedin.com/profile/view?id=5907154&authType=NAME_SEARCH&authToken=pSpl&locale=en_US&srchid=32272301421438011111&srchindex=1&srchtotal=1&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A32272301421438011111%2CVSRPtargetId%3A5907154%2CVSRPcmpt%3Aprimary), senior director of product marketing at DataDirect Networks, has over 12 years experience creating and driving enterprise hardware, software and professional services offerings and effectively bringing them to market. Jeff is often quoted in storage industry publications for his expertise in software-defined storage and moving beyond traditional approaches to decouple performance from capacity.

### Speaker Organization

DataDirect Networks

### Presentation

<iframe src="https://openpowerfoundation.org/wp-content/uploads/2015/03/Sisilli_OPFS2015_031815.pdf" width="100%" height="450" frameborder="0"></iframe>

 [Download Presentation](https://openpowerfoundation.org/wp-content/uploads/2015/03/Sisilli_OPFS2015_031815.pdf)

[Back to Summit Details](javascript:history.back())
