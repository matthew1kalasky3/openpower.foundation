---
title: IBM NIC 5899 5260 4 port 1Gb Ethernet
vendor: IBM
image: ibmnic58995260.bmp
tags:
  - product
  - ethernet
  - networkinterfacecard
date: 2016-04-05
draft: false
---

The IBM NIC is a 4 port 1Gbps Ethernet card based upon the Broadcom® NetXtreme® BCM 5719 PCIe2 4-port 1GbE Adapter.
